INSERT INTO `limitation_language` (`limitation_id`, `lang_code`, `name`) VALUES
('A001', 'NE', 'राम्रो दृष्टि हुनु पर्ने! '),
('A002', 'NE', 'शारीरिक क्षमता  '),
('A003', 'NE', 'छिटो-छिटो औला चलाउने क्षमता '),
('A004', 'NE', 'राम्रोसंग सुन्ने क्षमता	'),
('A005', 'NE', 'उच्च स्वाद र वासनाको क्षमता '),
('A006', 'NE', 'दृढ कदम '),
('G001', 'NE', 'महिलाहरुको पहुँचमा छैन  '),
('G002', 'NE', 'महिलाहरुका लागि कठिन(गाह्रो) छ'),
('G003', 'NE', 'पुरुषहरुको पहुँचमा छैन '),
('G004', 'NE', 'पुरुषहरुका लागि कठिन (गाह्रो) छ '),
('W001', 'NE', 'धेरै गर्मि वा चिसो '),
('W002', 'NE', 'धेरै शारीरिक काम '),
('W003', 'NE', 'पूर्णरुपमा स्क्रिन कार्य'),
('W004', 'NE', 'पूर्ण रुपमा ग्राहक संग सम्बधित '),
('W005', 'NE', 'निर्धारित कार्य समय छैन'),
('W006', 'NE', 'सिफ्टमा काम गर्न '),
('W007', 'NE', 'तनाब वा दवावमा काम गर्ने'),
('W008', 'NE', 'फोहोर र हल्ला हुने कार्यस्थल '),
('W009', 'NE', 'बाहिर हिडी रहने काम'),
('W010', 'NE', 'बच्चा, अपाङ्ग र वृद्धहरु संग काम');
